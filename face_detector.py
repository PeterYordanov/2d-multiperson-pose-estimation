import dlib
import cv2
from imutils import face_utils
from config import POINT_COLOR, POINT_RADIUS, POINT_THICKNESS
import imutils

class FaceDetector:    
    def __init__(self, face_shape_predictor):  
        self.detector = dlib.get_frontal_face_detector()
        self.predictor = dlib.shape_predictor(face_shape_predictor)

    def shape_to_np(shape, dtype='int'):
	    coords = np.zeros((68, 2), dtype=dtype)
	    for i in range(0, 68):
		    coords[i] = (shape.part(i).x, shape.part(i).y)
	    return coords    

    def detect(self, matrix):
        matrix = imutils.resize(matrix, width=500)
        gray = cv2.cvtColor(matrix, cv2.COLOR_BGR2GRAY)

        rects = self.detector(gray, 1)
        for (i, rect) in enumerate(rects):
	        shape = self.predictor(gray, rect)
	        shape = face_utils.shape_to_np(shape)
	        (x, y, w, h) = face_utils.rect_to_bb(rect)
	        
	        for (x, y) in shape:
		        cv2.circle(matrix, 
                          (x, y), 
                          POINT_RADIUS, 
                          POINT_COLOR, 
                          POINT_THICKNESS)
        
        return matrix
    
    def __del__(self):
        pass

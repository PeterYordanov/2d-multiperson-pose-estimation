#!/usr/bin/python3
# -*- coding: utf-8 -*-
import logging
from typing import Optional

_logger: Optional[logging.Logger] = None

def get_logger() -> logging.Logger:
    global _logger
    if _logger is None:
        raise RuntimeError('You need to setup the logger first')
    return _logger

def set_logger(name:str, level=logging.DEBUG) -> None:
    global _logger
    if _logger is not None:
        raise RuntimeError('Logger has already been setup')

    _logger = logging.getLogger(name)
    _logger.handlers.clear()
    _logger.setLevel(level)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(level)
    console_handler.setFormatter(_get_formatter())
    _logger.addHandler(console_handler)
    _logger.propagate = False


def _get_formatter() -> logging.Formatter:
    return logging.Formatter(
        '[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')

set_logger('Pose Estimation')

# 2D Multi-Person Pose Estimation

[[_TOC_]]

## CI/CD
[![pipeline status](https://gitlab.com/PeterYordanov/2d-multiperson-pose-estimation/badges/master/pipeline.svg)](https://gitlab.com/PeterYordanov/2d-multiperson-pose-estimation/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
  
## Features
- [x] Pose Estimation
- [x] Hand Estimation
- [x] Face Detection
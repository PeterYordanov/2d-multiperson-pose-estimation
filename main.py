from __future__ import division
import cv2
import time
import numpy as np
from random import randint
from pose_estimator import PoseEstimator
from hand_estimator import HandEstimator
from face_detector import FaceDetector
import argparse

if __name__ == '__main__':
    ap = argparse.ArgumentParser()
    ap.add_argument('-i', '--image_path', required=True, help='Path to the image to load', default='res/images/hand.png')
    ap.add_argument('-cp', '--coco_pose', required=False, help='', default='res/assets/coco/pose_deploy_linevec.prototxt')
    ap.add_argument('-cw', '--coco_weights', required=False, help='', default='res/assets/coco/pose_iter_440000.caffemodel')
    ap.add_argument('-fp', '--face_predictor', required=False, help='', default='res/assets/shape/shape_predictor_68_face_landmarks.dat')
    ap.add_argument('-hp', '--hand_pose', required=False, help='', default='res/assets/hand_pose/pose_deploy.prototxt')
    ap.add_argument('-hw', '--hand_weights', required=False, help='', default='res/assets/hand_pose/pose_iter_102000.caffemodel')

    args = vars(ap.parse_args())

    image = cv2.imread(args['image_path'])
    estimator = PoseEstimator(args['coco_pose'], args['coco_weights'])
    face = FaceDetector(args['face_predictor'])
    hand = HandEstimator(args['hand_pose'], args['hand_weights'])

    frameClone = estimator.estimate(image)
    frameClone = face.detect(frameClone)
    frameClone = hand.estimate(frameClone)

    cv2.imshow('Window', frameClone)
    cv2.waitKey(0)

    cv2.destroyAllWindows()
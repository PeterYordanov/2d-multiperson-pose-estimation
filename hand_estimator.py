from __future__ import division
import cv2
import time
import numpy as np
from config import LINE_COLOR, POINT_COLOR, LINE_THICKNESS, LINE_COLOR, HAND_POSE_PAIRS, POINT_RADIUS, POINT_THICKNESS


class HandEstimator:    
    def __init__(self, hand_pose, caffe_weights):  
        self.net = cv2.dnn.readNetFromCaffe(hand_pose, caffe_weights)
    
    def estimate(self, matrix):
        nPoints = 22

        frameCopy = np.copy(matrix)
        frameWidth = matrix.shape[1]
        frameHeight = matrix.shape[0]
        aspect_ratio = frameWidth / frameHeight

        threshold = 0.1

        inHeight = 368
        inWidth = int(((aspect_ratio * inHeight) * 8) // 8)
        inpBlob = cv2.dnn.blobFromImage(matrix, 1.0 / 255, (inWidth, inHeight), (0, 0, 0), swapRB=False, crop=False)

        self.net.setInput(inpBlob)

        output = self.net.forward()

        points = []

        for i in range(nPoints):
            probMap = output[0, i, :, :]
            probMap = cv2.resize(probMap, (frameWidth, frameHeight))
            
            minVal, prob, minLoc, point = cv2.minMaxLoc(probMap)

            if prob > threshold :
                cv2.circle(frameCopy, 
                          (int(point[0]), int(point[1])), 
                          POINT_RADIUS, 
                          POINT_COLOR, 
                          POINT_THICKNESS, 
                          cv2.FILLED)

                points.append((int(point[0]), int(point[1])))
            else :
                points.append(None)

        for pair in HAND_POSE_PAIRS:
            partA = pair[0]
            partB = pair[1]

            if points[partA] and points[partB]:
                cv2.line(matrix, points[partA], points[partB], LINE_COLOR, LINE_THICKNESS)
                cv2.circle(matrix, points[partA], POINT_RADIUS, POINT_COLOR, POINT_THICKNESS, cv2.FILLED)
                cv2.circle(matrix, points[partB], POINT_RADIUS, POINT_COLOR, POINT_THICKNESS, cv2.FILLED)

        return matrix
    
    def __del__(self):
        pass

import cv2
import time
import numpy as np
from random import randint
from config import BODY_POSE_PAIRS, MAP_INDEXES, KEYPOINTS_MAPPING, LINE_COLOR, POINT_COLOR, LINE_THICKNESS, LINE_COLOR, HAND_POSE_PAIRS, POINT_RADIUS, POINT_THICKNESS

class PoseEstimator:    
    def __init__(self, coco_pose, caffe_weights):  
        self.detected_keypoints = []
        self.keypoints_list = np.zeros((0,3))
 
        self.net = cv2.dnn.readNetFromCaffe(coco_pose, caffe_weights)
    
    def get_valid_pairs(self, output):
        valid_pairs = []
        invalid_pairs = []
        n_interp_samples = 10
        paf_score_th = 0.1
        conf_th = 0.7

        for k in range(len(MAP_INDEXES)):
            pafA = output[0, MAP_INDEXES[k][0], :, :]
            pafB = output[0, MAP_INDEXES[k][1], :, :]
            pafA = cv2.resize(pafA, (self.frameWidth, self.frameHeight))
            pafB = cv2.resize(pafB, (self.frameWidth, self.frameHeight))

            candA = self.detected_keypoints[BODY_POSE_PAIRS[k][0]]
            candB = self.detected_keypoints[BODY_POSE_PAIRS[k][1]]
            nA = len(candA)
            nB = len(candB)

            if(nA != 0 and nB != 0):
                valid_pair = np.zeros((0,3))
                for i in range(nA):
                    max_j=-1
                    maxScore = -1
                    found = 0
                    for j in range(nB):
                        d_ij = np.subtract(candB[j][:2], candA[i][:2])
                        norm = np.linalg.norm(d_ij)
                        if norm:
                            d_ij = d_ij / norm
                        else:
                            continue

                        interp_coord = list(zip(np.linspace(candA[i][0], candB[j][0], num=n_interp_samples),
                                                np.linspace(candA[i][1], candB[j][1], num=n_interp_samples)))

                        paf_interp = []
                        for k in range(len(interp_coord)):
                            paf_interp.append([pafA[int(round(interp_coord[k][1])), int(round(interp_coord[k][0]))],
                                               pafB[int(round(interp_coord[k][1])), int(round(interp_coord[k][0]))] ])

                        paf_scores = np.dot(paf_interp, d_ij)
                        avg_paf_score = sum(paf_scores)/len(paf_scores)

                        if (len(np.where(paf_scores > paf_score_th)[0]) / n_interp_samples ) > conf_th :
                            if avg_paf_score > maxScore:
                                max_j = j
                                maxScore = avg_paf_score
                                found = 1
                    if found:
                        valid_pair = np.append(valid_pair, [[candA[i][3], candB[max_j][3], maxScore]], axis=0)

                valid_pairs.append(valid_pair)
            else:
                invalid_pairs.append(k)
                valid_pairs.append([])
                
        return valid_pairs, invalid_pairs
    
    def get_keypoints(self, probMap, threshold=0.1):
        mapSmooth = cv2.GaussianBlur(probMap,(3,3),0,0)

        mapMask = np.uint8(mapSmooth>threshold)
        keypoints = []

        contours, hierarchy = cv2.findContours(mapMask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        for cnt in contours:
            blobMask = np.zeros(mapMask.shape)
            blobMask = cv2.fillConvexPoly(blobMask, cnt, 1)
            maskedProbMap = mapSmooth * blobMask
            _, maxVal, _, maxLoc = cv2.minMaxLoc(maskedProbMap)
            keypoints.append(maxLoc + (probMap[maxLoc[1], maxLoc[0]],))

        return keypoints
    
        
    def get_personwise_keypoints(self, valid_pairs, invalid_pairs):
        personwiseKeypoints = -1 * np.ones((0, 19))

        for k in range(len(MAP_INDEXES)):
            if k not in invalid_pairs:
                partAs = valid_pairs[k][:,0]
                partBs = valid_pairs[k][:,1]
                indexA, indexB = np.array(BODY_POSE_PAIRS[k])

                for i in range(len(valid_pairs[k])):
                    found = 0
                    person_idx = -1
                    for j in range(len(personwiseKeypoints)):
                        if personwiseKeypoints[j][indexA] == partAs[i]:
                            person_idx = j
                            found = 1
                            break

                    if found:
                        personwiseKeypoints[person_idx][indexB] = partBs[i]
                        personwiseKeypoints[person_idx][-1] += self.keypoints_list[partBs[i].astype(int), 2] + valid_pairs[k][i][2]

                    elif not found and k < 17:
                        row = -1 * np.ones(19)
                        row[indexA] = partAs[i]
                        row[indexB] = partBs[i]
                        row[-1] = sum(self.keypoints_list[valid_pairs[k][i,:2].astype(int), 2]) + valid_pairs[k][i][2]
                        personwiseKeypoints = np.vstack([personwiseKeypoints, row])
                        
        return personwiseKeypoints
    
    def estimate(self, matrix):
        nPoints = 18

        self.frameWidth = matrix.shape[1]
        self.frameHeight = matrix.shape[0]

        t = time.time()

        inHeight = 368
        inWidth = int((inHeight / self.frameHeight) * self.frameWidth)

        inpBlob = cv2.dnn.blobFromImage(matrix, 1.0 / 255, (inWidth, inHeight),
                                  (0, 0, 0), swapRB=False, crop=False)

        self.net.setInput(inpBlob)
        output = self.net.forward()

        keypoint_id = 0
        threshold = 0.1

        for part in range(nPoints):
            probMap = output[0,part,:,:]
            probMap = cv2.resize(probMap, (matrix.shape[1], matrix.shape[0]))
            keypoints = self.get_keypoints(probMap, threshold)            
            keypoints_with_id = []
            for i in range(len(keypoints)):
                keypoints_with_id.append(keypoints[i] + (keypoint_id,))
                self.keypoints_list = np.vstack([self.keypoints_list, keypoints[i]])
                keypoint_id += 1

            self.detected_keypoints.append(keypoints_with_id)

        frameClone = matrix.copy()
        for i in range(nPoints):
            for j in range(len(self.detected_keypoints[i])):
                cv2.circle(frameClone, 
                          self.detected_keypoints[i][j][0:2], 
                          POINT_RADIUS, 
                          POINT_COLOR, 
                          POINT_THICKNESS, 
                          cv2.LINE_AA)

        valid_pairs, invalid_pairs = self.get_valid_pairs(output)
        personwiseKeypoints = self.get_personwise_keypoints(valid_pairs, invalid_pairs)

        for i in range(17):
            for n in range(len(personwiseKeypoints)):
                index = personwiseKeypoints[n][np.array(BODY_POSE_PAIRS[i])]
                if -1 in index:
                    continue
                B = np.int32(self.keypoints_list[index.astype(int), 0])
                A = np.int32(self.keypoints_list[index.astype(int), 1])
                cv2.line(frameClone, (B[0], A[0]), (B[1], A[1]), LINE_COLOR, LINE_THICKNESS, cv2.LINE_AA)
                
        return frameClone
                    
    def __del__(self):
        self.str = ''